<?php
/**
 * Created by EK
 * User: ek
 * Date: 10.03.2017
 * Time: 10:43
 */

namespace XlsData;

use XlsData\Methods\Xls;
use XlsData\Methods\XlsHeader;

class Parser
{
    private $_static_data = [];

    protected $file;
    protected $config;
    protected $iterations = 500;
    protected $error = [];
    protected $_events = [];

    const DATA_TYPE_DATE = 1;
    const DATA_TYPE_SEARCH_DATE = 2;
    const DATA_TYPE_FORMULA = 3;

    /**
     * Parser constructor.
     *
     * @param $file
     * @param $config
     *
     * Config example
     *  'client'                 => 'Client (Name)',
     *  'date_of_sale'           => [
     *      'name' => 'Дата продажи',
     *      'type' => Parser::DATA_TYPE_DATE,
     *      'callback' => function($cell, $parsedVal){ return $parsedVal; }
     *      'required' => false,
     *  ],
     * 'date_of_sale'           => [
     *      'name' => ['Дата продажи', 'Дата этой продажи'],
     *      'type' => Parser::DATA_TYPE_DATE,
     *      'format' => 'Y-m-d',
     *      'save_last_row' => true // if need to save previous row value
     *      'callback' => function($cell, $parsedVal){ return $parsedVal; }
     *  ],
     * 'date_to_search' => [ // example "10 12 2017г. "
     *      'name' => ['Дата продажи', 'Дата этой продажи'],
     *      'type' => Parser::DATA_TYPE_SEARCH_DATE,
     *      'formats' => ['Y-m-d', 'd.m.Y'],
     *      'save_last_row' => true // if need to save previous row value
     *  ],
     * 'required' => true // Is field required? Default is TRUE
     *
     * @throws Exception
     */
    public function __construct($file, $config)
    {
        if (!is_string($file)) {
            throw new Exception('Param is not a string');
        }
        if (!is_file($file)) {
            throw new Exception('File not found');
        }
        if (!is_array($config)) {
            throw new Exception('File not found');
        }

        $this->file   = $file;
        $this->config = $config;
    }

    public function registerEvent($eventName, Method $method, Parser $parser, &$data)
    {
        foreach ($this->_events as $event) {
            if ($event['name'] == $eventName) {
                $event['func']($method, $parser, $data);
            }
        }
    }

    public function addEventFunction($eventName, $func)
    {
        $this->_events[] = [
            'name' => $eventName,
            'func' => $func,
        ];
    }

    public function addStaticData($name, $value)
    {
        $this->_static_data[$name] = $value;
    }

    public function getStaticData($name = null)
    {
        if ($name) {
            if (isset($this->_static_data[$name])) {
                return $this->_static_data[$name];
            } else {
                return false;
            }
        }

        return $this->_static_data;
    }

    public function parseXlsWithHeader()
    {
        $method = new XlsHeader($this);

        return $this->parse($method);
    }

    public function parseXls()
    {
        $method = new Xls($this);

        return $this->parse($method);
    }

    public function parseBy($method)
    {
        return $this->parse($method);
    }

    protected function parse($method)
    {
        return $method->parse();
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param $value
     *
     * @throws Exception
     */
    public function setIterations($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('Values must be a number');
        }
        $this->iterations = $value;
    }

    /**
     * @return int
     */
    public function getIterations()
    {
        return $this->iterations;
    }

    /**
     * @param string $errorMessage
     */
    public function addError($errorMessage)
    {
        $this->error[] = $errorMessage;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->error;
    }

}
