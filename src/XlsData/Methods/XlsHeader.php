<?php
/**
 * Created by Kartoteka team.
 * User: ek
 * Date: 15.11.2017
 */

namespace XlsData\Methods;


use console\helpers\Debug;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use XlsData\Method;
use XlsData\Parser;

class XlsHeader extends Method
{
    private $previousColValue = [];
    private $_before_header   = false;
    private $_headRow         = false;
    private $_activeWorkSheet;

    private $_rowIterator;

    protected $excelObject     = false;
    protected $headerAssociate = [];

    /**
     * @param \PHPExcel $obj
     */
    protected function setEObject(\PhpOffice\PhpSpreadsheet\Spreadsheet $obj)
    {
        $this->excelObject = $obj;
    }

    /**
     * @return \PHPExcel
     */
    protected function getEObject()
    {
        return $this->excelObject;
    }

    /**
     * @param $string
     *
     * @return string
     */
    protected function clearHeadString($string)
    {
        $value = mb_strtolower(trim(trim(strtr($string, [' ' => '', '.' => '', ',' => '', '!' => '', '-' => '', '_' => '', '(' => '', ')' => '', ':' => '', "\n" => '', "\r" => ''])), "'"));

        return $value;
    }

    protected function isNeededColumn($columnName)
    {
        return isset($this->headerAssociate[$columnName]);
    }

    protected function getColumnConfig($columnName)
    {
        $config = $this->parser->getConfig();
        $params = $config[$this->getColumnLabel($columnName)];

        return $params;
    }

    /**
     * @param array $rowData
     */
    protected function getAssociatedArray($rowData)
    {
        $resultData = [];
        foreach ($this->headerAssociate as $k => $v) {
            $resultData[$v] = !is_string($rowData[$k]) ? $rowData[$k] : $this->deleteBadSymbols($rowData[$k]);
        }

        return $resultData;
    }

    /**
     * @param $colName
     *
     * @return string
     * @throws \Exception
     */
    protected function getColumnLabel($colName)
    {
        if (empty($this->headerAssociate[$colName])) {
            throw new \Exception('Check header. Can`t find col name ' . $colName);
        }

        return $this->headerAssociate[$colName];
    }

    protected function parseHead($worksheet)
    {
        $lastValues = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $values = [];

            foreach ($cellIterator as $cell) {
                $value = $cell->getCalculatedValue();
                if (!$value) {
                    continue;
                }

                $values[$cell->getColumn()] = $value;
            }

            $this->parser->registerEvent('before_header_line', $this, $this->parser, $values);

            foreach ($values as $key => $value) {
                $value = $this->clearHeadString($value);
                if (!$value) {
                    continue;
                }

                $values[$key] = $value;
            }

            $requiredColumns = [];
            foreach ($this->parser->getConfig() as $key => $confRow) {
                if (!(isset($confRow['required']) && $confRow['required'] === false)) {
                    $requiredColumns[$key] = $confRow['name'];
                }
            }

            if (!empty($lastValues) && count($values) < count($requiredColumns) && count($values) < count($lastValues)) {
                foreach ($values as $k => $v) {
                    $lastValues[$k] = $v;
                }
                $values = $lastValues;
            }

            if (!empty($lastValues)) {
                foreach ($lastValues as $k => $v) {
                    if (empty($values[$k]) && !empty($lastValues[$k])) {
                        $values[$k] = $v;
                    }
                }
            }

//            var_dump($lastValues);
//            var_dump($values);
//            var_dump(count($values) >= count($requiredColumns));

            $checkRequeiredHeadValues = function ($values, $requiredColumns) {

                $rcOrigin = $requiredColumns;
                foreach ($requiredColumns as $rk => $rc) {
                    if (is_array($rc)) {
                        foreach ($rc as $oneOfColumnNames) {
                            foreach ($values as $v) {
                                if ($this->clearHeadString($oneOfColumnNames) == $this->clearHeadString($v) && isset($rcOrigin[$rk])) {
                                    unset($rcOrigin[$rk]);
                                }
                            }
                        }
                    } else {
                        foreach ($values as $v) {
                            if ($this->clearHeadString($rc) == $this->clearHeadString($v) && isset($rcOrigin[$rk])) {
                                unset($rcOrigin[$rk]);
                            }
                        }
                    }
                }

                return empty($rcOrigin);
            };

            if ($checkRequeiredHeadValues($values, $requiredColumns)) {
                $headerAssociate = [];
                foreach ($this->parser->getConfig() as $name => $params) {
                    if (is_array($params)) {
                        if (empty($params['name'])) {
                            throw new \Exception('Not set name for param ' . $name);
                        }
                        $colName = $params['name'];
                    } else {
                        $colName = $params;
                    }

                    foreach ($values as $c => $v) {
                        if (is_array($colName)) {
                            foreach ($colName as $oneOfColumnNames) {
                                if ($this->clearHeadString($oneOfColumnNames) == $this->clearHeadString($v)) {
                                    $headerAssociate[$c] = $name;
                                }
                            }
                        } else {
                            if ($this->clearHeadString($colName) == $this->clearHeadString($v)) {
                                $headerAssociate[$c] = $name;
                            }
                        }
                    }
                }

                if (count($headerAssociate) >= count($requiredColumns)) {
                    $this->headerAssociate = $headerAssociate;

                    return $row->getRowIndex();
                }
            }

            if ($row->getRowIndex() >= 20) {
                $this->parser->addError('Шапка файла не распознана');

                return false;
            }
            $lastValues = $values;
        }
    }

    public function checkFileHeader()
    {
        $this->setEObject(\PhpOffice\PhpSpreadsheet\IOFactory::load($this->parser->getFile()));

        foreach ($this->getEObject()->getWorksheetIterator() as $worksheet) {
            if (($rowIndex = $this->parseHead($worksheet)) === false) {
                continue;
            }

            return $this->headerAssociate;
        }
    }

    public function getHead()
    {
        if ($this->_headRow !== false) {
            return $this->_headRow;
        }

        $this->setEObject(\PhpOffice\PhpSpreadsheet\IOFactory::load($this->parser->getFile()));

        foreach ($this->getEObject()->getWorksheetIterator() as $worksheet) {

            $this->_activeWorkSheet = $worksheet;

            if (($rowIndex = $this->parseHead($worksheet)) === false) {
                continue;
            }

            return $this->_headRow = $rowIndex;
        }
    }

    public function parse()
    {
        $rowIndex = $this->getHead();

        return $this->getDataFromRowIndex($this->_activeWorkSheet, $rowIndex);
    }

    public function parseIterator($limits = 500)
    {
        $this->setEObject(\PhpOffice\PhpSpreadsheet\IOFactory::load($this->parser->getFile()));

        $rowIndex    = $this->getHead();
        $parsedLines = 0;

        $data = [];
        while ($parsedLines < $limits) {
            $parsedLines++;

            $rowData  = $this->getDataFromRowIndex($this->_activeWorkSheet, $rowIndex, true);
            $rowIndex = $this->_headRow++;

            if (is_array($rowData) && !empty($rowData)) {
                $data[] = $rowData;
            }
        }

        return $data;
    }

    /**
     * @param \PHPExcel_Cell $cell
     * @param array $config
     *
     * @return mixed
     */
    protected function getFormatedValue($cell, $config)
    {
        switch ($config['type']) {
            case Parser::DATA_TYPE_DATE:
                if (empty($config['format'])) {
                    throw new \Exception('Set the DATE FORMAT "format"');
                }

                $normValue = trim($cell->getValue());

                if (is_numeric($normValue) && $normValue < 10000000 && \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell) && is_numeric($normValue)) {
                    return date('Y-m-d H:i:s', \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($normValue));
                } else {
                    if (empty($normValue)) {
                        return '';
                    }

                    $date = \DateTime::createFromFormat($config['format'], $this->deleteBadSymbols($normValue));

                    if (!$date && isset($config['format_second'])) {
                        $date = \DateTime::createFromFormat($config['format_second'], $this->deleteBadSymbols($normValue));
                    }

                    if (!$date) {
                        throw new \Exception('Wrong date format in file. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }
                    if (\DateTime::getLastErrors()['warning_count'] > 0) {
                        throw new \Exception('The parsed date was invalid. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }
                    if ($date->format('Y') < 2016 || $date->format('Y') > date('Y + one year')) {
                        throw new \Exception('The parsed date was invalid. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }

                    return $date->format('Y-m-d H:i:s');
                }
            case Parser::DATA_TYPE_SEARCH_DATE:
                $normValue = trim($cell->getValue());

                if (is_numeric($normValue) && $normValue < 10000000 && \PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell) && is_numeric($normValue)) {
                    return date('Y-m-d H:i:s', \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($normValue));
                } else {
                    if (empty($normValue)) {
                        return '';
                    }

                    $normValue = preg_replace('~[^0-9./]+~', '', $normValue);

                    if (!empty($config['formats']) && is_array($config['formats'])) {
                        $date = null;
                        foreach ($config['formats'] as $format) {
                            $date = \DateTime::createFromFormat($format, $this->deleteBadSymbols($normValue));
                            if ($date) {
                                break;
                            }
                        }

                        if ($date && !\DateTime::getLastErrors()['warning_count']) {
                            return $date->format('Y-m-d H:i:s');
                        }
                    }

                    $time = strtotime($normValue);
                    if ($time) {
                        return date('Y-m-d H:i:s', $time);
                    }

                    return false;
                }
            case Parser::DATA_TYPE_FORMULA:
                return $cell->getCalculatedValue();
            default:
                return $cell->getValue();
        }
    }

    /**
     * @param \PHPExcel_Worksheet $worksheet
     * @param int $rowIndex
     *
     * @return array
     */
    protected function getDataFromRowIndex($worksheet, $rowIndex = 0, $oneRow = false)
    {
        if (!$rowIndex) {
            $rowIndex = 0;
        }

//        Debug::info('Get xlsx data from row #' . $rowIndex);
        if ($this->_rowIterator === null) {
//            Debug::info('get row iterator');
            $this->_rowIterator = $worksheet->getRowIterator($rowIndex);
        }

        $data = [];
        foreach ($this->_rowIterator as $row) {

            if ($rowIndex >= $row->getRowIndex()) {
                continue;
            }

            $rowData = [];

            $cellIterator = $row->getCellIterator();
//            $cellIterator->setIterateOnlyExistingCells(true); // Loop all cells, even if it is not set

            $emptyLine = true;

            foreach ($cellIterator as $cell) {
                if (!$this->isNeededColumn($cell->getColumn())) {
                    continue;
                }

                $config = $this->getColumnConfig($cell->getColumn());
                if (is_array($config) && !empty($config['type'])) {
                    $val = $this->getFormatedValue($cell, $config);
                } elseif ($cell->getCalculatedValue()) {
                    $val = $cell->getCalculatedValue();
                } else {
                    $val = $cell->getValue();
                }

                if ($val instanceof RichText) {
                    $val = $val->getPlainText();
                }

                if (is_array($config) && !empty($config['callback'])) {
                    $val = $config['callback']($cell, $val);
                }

                $rowData[$cell->getColumn()] = $val;

                if (!empty($val)) {
                    $emptyLine = false;
                }

                if (!empty($config['save_last_row'])) {
                    if (empty($val) && !empty($this->previousColValue[$cell->getColumn()])) {
                        $val = $this->previousColValue[$cell->getColumn()];
                    }
                }
                if (!empty($val)) {
                    $this->previousColValue[$cell->getColumn()] = $val;
                }

                $rowData[$cell->getColumn()] = $val;
            }

            $resultData = [];

            if (!$emptyLine) {
                $resultData = $this->getAssociatedArray($rowData);
                $this->parser->registerEvent('after_parse_line', $this, $this->parser, $resultData);

                if (!empty($resultData)) {
                    $data[] = $resultData;
                }
            }

            if ($oneRow) {
                return $resultData;
            }
        }

        if ($oneRow) {
            return false;
        }

        return $data;
    }
}