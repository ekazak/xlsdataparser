<?php
/**
 * Created by Kartoteka team.
 * User: ek
 * Date: 15.11.2017
 */

namespace XlsData\Methods;


use XlsData\Method;
use XlsData\Parser;

class XlsMultyHeader extends XlsHeader
{
    protected $previousColValue = [];
    protected $_before_header = false;

    protected $excelObject = false;
    protected $headerAssociate = [];

    /**
     * @param \PHPExcel $obj
     */
    protected function setEObject(\PHPExcel $obj)
    {
        $this->excelObject = $obj;
    }

    /**
     * @return \PHPExcel
     */
    protected function getEObject()
    {
        return $this->excelObject;
    }

    /**
     * @param $string
     *
     * @return string
     */
    protected function clearHeadString($string)
    {
        $value = mb_strtolower(trim(trim(strtr($string, array(' ' => '', '.' => '', ',' => '', '!' => '', '-' => '', '_' => '', '(' => '', ')' => '', ':' => '', "\n" => '', "\r" => ''))), "'"));

        return $value;
    }

    protected function isNeededColumn($columnName)
    {
        return isset($this->headerAssociate[$columnName]);
    }

    protected function getColumnConfig($columnName)
    {
        $config = $this->parser->getConfig();
        $params = [];
        foreach ($this->getColumnLabel($columnName) as $name) {
            $params[$name] = $config[$name];
        }


        return $params;
    }

    /**
     * @param $colName
     *
     * @return string
     * @throws \Exception
     */
    protected function getColumnLabel($colName)
    {
        if (empty($this->headerAssociate[$colName])) {
            throw new \Exception('Check header. Can`t find col name ' . $colName);
        }

        return $this->headerAssociate[$colName];
    }

    protected function parseHead($worksheet)
    {
        $lastValues = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $values = [];

            foreach ($cellIterator as $cell) {
                $value = $cell->getCalculatedValue();
                if (!$value) {
                    continue;
                }

                $values[$cell->getColumn()] = $value;
            }

            $this->parser->registerEvent('before_header_line', $this, $this->parser, $values);

            foreach ($values as $key => $value) {
                $value = $this->clearHeadString($value);
                if (!$value) {
                    continue;
                }

                $values[$key] = $value;
            }

            $requiredColumns = [];
            foreach ($this->parser->getConfig() as $key => $confRow) {
                if (!(isset($confRow['required']) && $confRow['required'] === false)) {
                    $requiredColumns[$key] = $confRow['name'];
                }
            }

            if (!empty($lastValues) && count($values) < count($requiredColumns)) {
                foreach ($values as $k => $v) {
                    $lastValues[$k] = $v;
                }
                $values = $lastValues;
            }

            d($values);

            if (count($values) >= count($requiredColumns)) {
                $headerAssociate = [];
                foreach ($this->parser->getConfig() as $name => $params) {
                    if (is_array($params)) {
                        if (empty($params['name'])) {
                            throw new \Exception('Not set name for param ' . $name);
                        }
                        $colName = $params['name'];
                    } else {
                        $colName = $params;
                    }

                    foreach ($values as $c => $v) {
                        if (is_array($colName)) {
                            foreach ($colName as $oneOfColumnNames) {
                                if ($this->clearHeadString($oneOfColumnNames) == $this->clearHeadString($v)) {
                                    $headerAssociate[$c][] = $name;
                                }
                            }
                        } else {
                            if ($this->clearHeadString($colName) == $this->clearHeadString($v)) {
                                $headerAssociate[$c][] = $name;
                            }
                        }
                    }
                }

                if (count($headerAssociate) >= count($requiredColumns)) {
                    $this->headerAssociate = $headerAssociate;

                    return $row->getRowIndex();
                }
            }

            if ($row->getRowIndex() >= 10) {
                $this->parser->addError('Шапка файла не распознана');

                return false;
            }
            $lastValues = $values;
        }
    }


    public function parse()
    {

        $inputFileType = \PHPExcel_IOFactory::identify($this->parser->getFile());
        $objReader     = \PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(false);

        $this->setEObject($objReader->load($this->parser->getFile()));

        foreach ($this->getEObject()->getWorksheetIterator() as $worksheet) {
            if (($rowIndex = $this->parseHead($worksheet)) === false) {
                continue;
            }

            return $this->getDataFromRowIndex($worksheet, $rowIndex);
        }
    }

    /**
     * @param \PHPExcel_Cell $cell
     * @param array          $config
     *
     * @return mixed
     */
    protected function getFormatedValue($cell, $config)
    {
        switch ($config['type']) {
            case Parser::DATA_TYPE_DATE:
                if (empty($config['format'])) {
                    throw new \Exception('Not set date format');
                }

                $normValue = $cell->getValue();

                if (is_numeric($normValue) && $normValue < 10000000 && \PHPExcel_Shared_Date::isDateTime($cell) && is_numeric($normValue)) {
                    return date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($normValue));
                } else {
                    if (empty($normValue)) {
                        return '';
                    }

                    $date = \DateTime::createFromFormat($config['format'], $this->deleteBadSymbols($normValue));

                    if (!$date && isset($config['format_second'])) {
                        $date = \DateTime::createFromFormat($config['format_second'], $this->deleteBadSymbols($normValue));
                    }

                    if (!$date) {
                        throw new \Exception('Wrong date format in file. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }
                    if (\DateTime::getLastErrors()['warning_count'] > 0) {
                        throw new \Exception('The parsed date was invalid. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }
                    if ($date->format('Y') < 2016 || $date->format('Y') > date('Y + one year')) {
                        throw new \Exception('The parsed date was invalid. Data: ' . $normValue . ' Format from config: ' . $config['format']);
                    }

                    return $date->format('Y-m-d H:i:s');
                }
            case Parser::DATA_TYPE_SEARCH_DATE:
                $normValue = $cell->getValue();

                if (is_numeric($normValue) && $normValue < 10000000 && \PHPExcel_Shared_Date::isDateTime($cell) && is_numeric($normValue)) {
                    return date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($normValue));
                } else {
                    if (empty($normValue)) {
                        return '';
                    }

                    $normValue = preg_replace('~[^0-9./]+~', '', $normValue);

                    if (!empty($config['formats']) && is_array($config['formats'])) {
                        $date = null;
                        foreach ($config['formats'] as $format) {
                            $date = \DateTime::createFromFormat($format, $this->deleteBadSymbols($normValue));
                            if ($date) {
                                break;
                            }
                        }

                        if ($date && !\DateTime::getLastErrors()['warning_count']) {
                            return $date->format('Y-m-d H:i:s');
                        }
                    }

                    $time = strtotime($normValue);
                    if ($time) {
                        return date('Y-m-d H:i:s', $time);
                    }

                    return false;
                }
            default:
                return $cell->getValue();
        }
    }

    /**
     * @param \PHPExcel_Worksheet $worksheet
     * @param int                 $rowIndex
     *
     * @return array
     */
    protected function getDataFromRowIndex($worksheet, $rowIndex = 0)
    {
        /**
         * @var $cell \PHPExcel_Cell
         */

        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {

            if ($rowIndex >= $row->getRowIndex()) {
                continue;
            }

            $rowData = [];

            $cellIterator = $row->getCellIterator();
//            $cellIterator->setIterateOnlyExistingCells(true); // Loop all cells, even if it is not set

            $emptyLine = true;

            foreach ($cellIterator as $cell) {

                if (!$this->isNeededColumn($cell->getColumn())) {
                    continue;
                }

                foreach ($this->getColumnConfig($cell->getColumn()) as $name => $config) {

                    if (is_array($config) && !empty($config['type'])) {
                        $val = $this->getFormatedValue($cell, $config);
                    } elseif ($cell->getCalculatedValue()) {
                        $val = $cell->getCalculatedValue();
                    } else {
                        $val = $cell->getValue();
                    }

                    if ($val instanceof \PHPExcel_RichText) {
                        $val = $val->getPlainText();
                    }

                    if (is_array($config) && !empty($config['callback'])) {
                        $val = $config['callback']($cell, $val);
                    }

                    $rowData[$name] = $val;

                    if (!empty($val)) {
                        $emptyLine = false;
                    }

                    if (!empty($config['save_last_row'])) {
                        if (empty($val) && !empty($this->previousColValue[$name])) {
                            $val = $this->previousColValue[$name];
                        }
                    }
                    if (!empty($val)) {
                        $this->previousColValue[$name] = $val;
                    }

                    $rowData[$name] = $this->deleteBadSymbols($val);
                }
            }

            if (!$emptyLine) {
                $this->parser->registerEvent('after_parse_line', $this, $this->parser, $rowData);

                if (!empty($rowData)) {
                    $data[] = $rowData;
                }
            }
        }

        return $data;
    }


}