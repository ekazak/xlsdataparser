<?php
/**
 * Created by Kartoteka team.
 * User: ek
 * Date: 15.11.2017
 */

namespace XlsData\Methods;


use XlsData\Method;
use XlsData\Parser;

class XlsMultyHeaderCells extends XlsMultyHeader
{
    /**
     * @param \PHPExcel_Worksheet $worksheet
     * @param int                 $rowIndex
     *
     * @return array
     */
    protected function getDataFromRowIndex($worksheet, $rowIndex = 0)
    {
        /**
         * @var $cell \PHPExcel_Cell
         */

        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {

            if ($rowIndex >= $row->getRowIndex()) {
                continue;
            }

            $rowData = [];

            $cellIterator = $row->getCellIterator();
//            $cellIterator->setIterateOnlyExistingCells(true); // Loop all cells, even if it is not set

            $emptyLine = true;

            $status = true;

//            echo 'LINE ' . $row->getRowIndex() . ' -------------------------------';

            foreach ($cellIterator as $cell) {

                if (!$this->isNeededColumn($cell->getColumn())) {
                    continue;
                }

                if ($cell->isInMergeRange()) {
                    $currentMergeCellsArray = \PHPExcel_Cell::splitRange($cell->getMergeRange());
                    $firstCellColumn        = substr($currentMergeCellsArray[0][0], 0, 1);
                    $secondCellColumn       = substr($currentMergeCellsArray[0][1], 0, 1);

                    if ($firstCellColumn != $secondCellColumn) {
                        break;
                    }

                    $cell = $worksheet->getCell($currentMergeCellsArray[0][0]);
                }

                foreach ($this->getColumnConfig($cell->getColumn()) as $name => $config) {

                    if (is_array($config) && !empty($config['type'])) {
                        $val = $this->getFormatedValue($cell, $config);
                    } elseif ($cell->getCalculatedValue()) {
                        $val = $cell->getCalculatedValue();
                    } else {
                        $val = $cell->getValue();
                    }

                    if ($val instanceof \PHPExcel_RichText) {
                        $val = $val->getPlainText();
                    }

                    if (is_array($config) && !empty($config['callback'])) {
                        $val = $config['callback']($cell, $val);
                    }

                    $rowData[$name] = $val;

                    if (!empty($val)) {
                        $emptyLine = false;
                    }

                    if ($val && !$this->isBlackColor($cell->getStyle()->getFont()->getColor()->getRGB())) {
                        $status = false;
                    }

                    $rowData[$name] = $this->deleteBadSymbols($val);
                }
            }

            if (!$emptyLine) {
                $rowData['color_status'] = $status;

                $this->parser->registerEvent('after_parse_line', $this, $this->parser, $rowData);

                if (!empty($rowData)) {
                    $data[] = $rowData;
                }
            }
        }

        return $data;
    }

    protected function isBlackColor($hex)
    {
        $r = substr($hex, 0, 2);
        $g = substr($hex, 2, 2);
        $b = substr($hex, 4, 2);

        $tr = base_convert($r, 16, 10);
        $tg = base_convert($g, 16, 10);
        $tb = base_convert($b, 16, 10);

        return ($tr < 50 && $tg < 50 && $tb < 50);
    }
}