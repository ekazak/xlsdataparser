<?php
/**
 * Created by Kartoteka team.
 * User: ek
 * Date: 15.11.2017
 */

namespace XlsData\Methods;

class Xls extends XlsHeader
{

    protected function parseHead($worksheet)
    {
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $values = [];

            $config = $this->parser->getConfig();

            foreach ($cellIterator as $cell) {
                if (empty($config)) {
                    break;
                }
                foreach ($config as $name => $value) {
                    $values[$cell->getColumn()] = $name;
                    array_shift($config);
                    break;
                }

            }

            $this->headerAssociate = $values;

            return 0;
        }

    }

}