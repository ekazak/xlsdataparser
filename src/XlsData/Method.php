<?php
/**
 * Created by Kartoteka team.
 * User: ek
 * Date: 15.11.2017
 */

namespace XlsData;

abstract class Method
{
    protected static $badSymbols = [
        '&nbsp;'  => ' ',
        '  '      => ' ',
        '   '     => ' ',
        '    '    => ' ',
        '&#160;'  => ' ',
        '&#132;'  => '&quot;',
        '&#148;'  => '&quot;',
        '&laquo;' => '&quot;',
        '&raquo;' => '&quot;',
        '&bdquo;' => '&quot;',
        '&rdquo;' => '&quot;',
        '&#39;'   => '&quot;',
        '«'       => '&quot;',
        '»'       => '&quot;',
    ];

    protected $parser;

    abstract public function parse();

    public function __construct(\XlsData\Parser $parser)
    {
        $this->parser = $parser;
    }

    protected function fixLine($str)
    {
        return $str;
    }

    public function deleteBadSymbols($str)
    {
        return htmlspecialchars_decode(trim(strtr(trim($str), self::$badSymbols)));
    }


}